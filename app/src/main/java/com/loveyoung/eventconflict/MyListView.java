package com.loveyoung.eventconflict;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

import androidx.annotation.Nullable;


/**
 * Author:created  By Walt-zhong at 2021/9/15 11:56
 * e-Mail:2511255880@qq.com
 */
@SuppressLint("AppCompatCustomView")
public class MyListView extends ListView {
    private static final String TAG = "MyTextView";
    private float mLastX;
    private float mLastY;

    public MyListView(Context context) {
        this(context,null);
    }

    public MyListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //在子view中进行冲突处理
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        float x = ev.getX();
//        float y = ev.getY();
//        switch (ev.getAction()){
//            case MotionEvent.ACTION_DOWN:
//                getParent().requestDisallowInterceptTouchEvent(true);
//                break;
//            case MotionEvent.ACTION_MOVE:
//                float xDiff = Math.abs(x-mLastX);
//                float yDiff = Math.abs(y-mLastY);
//
//                if(xDiff > yDiff){
//                    getParent().requestDisallowInterceptTouchEvent(false);
//                }
//
//                break;
//            default:break;
//
//        }
//
//        mLastX = x;
//        mLastY = y;
//        return super.dispatchTouchEvent(ev);
//    }
}