package com.loveyoung.eventconflict;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

public class MainActivity extends AppCompatActivity {

    List<View> mViews;
    List<Drawable> listData;
    private LayoutInflater layoutInflater;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initVPData();
        MyViewPager myVp = findViewById(R.id.my_vm);

        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return mViews.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
                container.removeView(mViews.get(position));
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup container, int position) {
                container.addView(mViews.get(position));
                return mViews.get(position);
            }
        };
        myVp.setAdapter(pagerAdapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initListData() {
        listData = new ArrayList<>();
        listData.add(getDrawable(R.mipmap.a1));
        listData.add(getDrawable(R.mipmap.a2));
        listData.add(getDrawable(R.mipmap.a3));
        listData.add(getDrawable(R.mipmap.a4));
        listData.add(getDrawable(R.mipmap.a5));
        listData.add(getDrawable(R.mipmap.a6));
        listData.add(getDrawable(R.mipmap.m4));
        listData.add(getDrawable(R.mipmap.m5));
        listData.add(getDrawable(R.mipmap.m7));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initVPData() {
        mViews = new ArrayList<>();
        initListData();
        layoutInflater = getLayoutInflater();
        View inflate = layoutInflater.inflate(R.layout.page1, null);
        MyListView myLv = inflate.findViewById(R.id.my_lv);
        myLv.setAdapter(new MyListAdapter());
        mViews.add(inflate);
        mViews.add(layoutInflater.inflate(R.layout.page2,null));
        mViews.add(layoutInflater.inflate(R.layout.page3,null));
    }


    class MyListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.lv_item, null);
                holder = new ViewHolder();
                holder.mIv = convertView.findViewById(R.id.my_iv);
                holder.mIv.setImageDrawable(listData.get(position));

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
                holder.mIv.setImageDrawable(listData.get(position));
            }

            return convertView;
        }

    }

    /**
     * 界面上的显示控件
     *
     * @author jiqinlin
     *
     */
    private static class ViewHolder{
        private ImageView mIv;
    }
}